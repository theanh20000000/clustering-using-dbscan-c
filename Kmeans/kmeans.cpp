#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include "../myLibary.cpp"

using namespace std;

bool ComparePointPositionVec(vector<PointPosition> a, vector<PointPosition> b){
  for(size_t i = 0; i < a.size(); ++i){
    if(a[i].x != b[i].x || a[i].y != b[i].y){
      return false;
    }
  }
  return true;
}
vector<vector<size_t>> CalKmean (const vector<PointPosition>& data, const size_t k) {
  vector<vector<size_t>> clusters(k);
  // Random k starting point
  vector<PointPosition> pre(k, PointPosition{0,0});
  for(PointPosition &i : pre){
    size_t j = RandomPoint(data.size() - 1);
    i = data[j];
  }
  vector<PointPosition> now(pre);

  do{
    for(vector<size_t> &cluster : clusters){
      cluster.clear();
    }
    // Clustering
    for(size_t i = 0; i < data.size(); ++i){
      double min = Distance(now[0], data[i]);
      size_t min_index = 0;
      for(size_t j = 1; j < pre.size(); ++j){
        double temp = Distance(now[j], data[i]);
        if(temp < min){
          min = temp;
          min_index = j;
        }
      }
      clusters[min_index].push_back(i);
    }
    // Update centroid
    pre = now;
    for(size_t i = 0; i < now.size(); ++i){
      double sum_x = 0;
      double sum_y = 0;
      vector<size_t> &cluster = clusters[i];
      for(const size_t &index : clusters[i]){
        sum_x += data[index].x;
        sum_y += data[index].y;
      }

      sum_x /= cluster.size();
      sum_y /= cluster.size();
      now[i].x = sum_x;
      now[i].y = sum_y;
    }
  } while (!ComparePointPositionVec(pre, now));
  return clusters;
}
int main(int argc, char *argv[]) {
  clock_t start = clock();
  ifstream data_file;
  data_file.open("data.txt");

  vector<PointPosition> data;

  if (data_file.is_open()) {
    string line;
    while (getline(data_file, line)) {
      PointPosition temp;
      temp = Split(line);
      data.push_back(temp);
    }
  }

  size_t k = stod(argv[1]);

  vector<vector<size_t>> result = CalKmean(data, k);
  ofstream data_out("Kmeans/output.txt");
  for(const auto &i:result){
    for(const auto &a:i){
      data_out << a << " ";
    }
    data_out<<endl;
  }
  data_out.close();

  clock_t end = clock();
  double elapsed = (double)(end - start);
  cout << "CPU Cycles: " << elapsed << " cycles" << endl;

  return 0;
}
