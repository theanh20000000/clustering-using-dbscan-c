#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <ctime>
#include "../myLibary.cpp"

using namespace std;

enum class PointType {kCore, kNonCore};
struct Point {
  PointPosition cordinate;
  PointType type = PointType::kNonCore;
};
// CalDistanceDBSCAN() return distance_matrix and modify data to set type to core or not
// distance_matrix form
// ~ a b c d
// a 0 ! ! !
// b 1 0 ! !
// c 1 1 0 !
// d 1 1 1 0
vector<vector<double>> CalDistanceDBSCAN (vector<Point> &data,const double radius, const size_t core_threshold) {
  vector<vector<double>> distance_matrix(data.size());
  for (size_t x = 0; x < data.size(); ++x) {
    distance_matrix[x] = vector<double>(x+1, 0);
  }
  for (size_t y = 0; y < data.size(); ++y) {
    // in_radius_count is -1 to exclude it self
    size_t in_radius_count = -1;
    for (size_t x = y; x < data.size(); ++x) {
      distance_matrix[x][y] = Distance(data[x].cordinate, data[y].cordinate);
      if (distance_matrix[x][y] <= radius) {
        ++in_radius_count;
      }
    }
    if (in_radius_count >= core_threshold) {
      data[y].type = PointType::kCore;
    }
  }
  return distance_matrix;
}
vector<vector<size_t>> Clustering (vector<Point>& data, const vector<vector<double>> distance_matrix, const double radius) {
  vector<size_t> already_assigned;
  stack<size_t> queue;
  vector<vector<size_t>> clusters;
  vector<size_t> cluster;
  vector<size_t> core_idx;
  // Get all core point
  for (size_t i = 0; i < data.size(); ++i) {
    if (data[i].type == PointType::kCore) {
      core_idx.push_back(i);
    }
  }
  // Expand from core point
  for (const size_t &i:core_idx) {
    if(MyFind(already_assigned, i)) {
      continue;
    }
    queue.push(i);
    while (!queue.empty()) {
      size_t idx = queue.top();
      queue.pop();
      already_assigned.push_back(idx);
      cluster.push_back(idx);
      for (size_t x = idx + 1; x< data.size(); ++x){
        if(distance_matrix[x][idx] <= radius && !MyFind(already_assigned, x)){
          if (data[x].type == PointType::kCore) {
            queue.push(x);
            already_assigned.push_back(x);
          }
          else{
            cluster.push_back(x);
            already_assigned.push_back(x);
          }
        }
      }
      for (size_t y = idx + 1; y--;){
        if(distance_matrix[idx][y] <= radius && !MyFind(already_assigned, y)){
          if (data[y].type == PointType::kCore){
            queue.push(y);
            already_assigned.push_back(y);
          }
          else {
            cluster.push_back(y);
            already_assigned.push_back(y);
          }
        }
      }
    }   
    clusters.push_back(cluster);
    cluster.clear();
  }
  return clusters;
}
int main(int argc, char *argv[]) {
  clock_t start = clock();
  ifstream data_file;
  data_file.open("data.txt");

  vector<Point> data;

  if (data_file.is_open()) {
    string line;
    while (getline(data_file, line)) {
      Point temp;
      temp.cordinate = Split(line);
      data.push_back(temp);
    }
  }
  double radius = stod(argv[1]);
  size_t core_threshold = stod(argv[2]);

  vector<vector<double>> distance_matrix = CalDistanceDBSCAN(data, radius, core_threshold);
  vector<vector<size_t>> result = Clustering(data, distance_matrix, radius);

  ofstream data_out("DBSCAN/output.txt");
  for(const auto &i:result){
    for(const auto &a:i){
      data_out << a << " ";
    }
    data_out<<endl;
  }
  data_out.close();

  clock_t end = clock();
  double elapsed = (double)(end - start);
  cout << "CPU Cycles: " << elapsed << " cycles" << endl;

  return 0;
}
