import matplotlib.pyplot as plt

data_point = []
point_idx = []
with open("OPTICS/reachability.txt", "r") as f:
    x = []
    y = []
    for i, line in enumerate(f):
        line = line.strip().split(' ')
        x.append(i)
        point_idx.append(int(line[0]))
        y.append(float(line[1]))
    y[0] = 0
    data_point = [x, y]
plt.title("OPTICS Reachability plot")
plt.bar(data_point[0], data_point[1])
plt.show()

cut_out_point = float(input("Input cut off point: "))
threshold = int(input("Input cluster threshold point: "))

cluster = []
clusters = []
for idx, id in enumerate(point_idx):
    if data_point[1][idx] < cut_out_point:
        cluster.append(id)
    else:
        clusters.append(cluster)
        cluster = []

clusters_fileted = [cluster for cluster in clusters if len(cluster) >= threshold]
with open("OPTICS/output.txt", "w+") as file:
    for clus in clusters_fileted:
        for index in clus:
            file.write(str(index)+" ")
        file.write("\n")
