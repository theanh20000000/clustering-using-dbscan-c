#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>
#include <iomanip>
#include "../myLibary.cpp"

using namespace std;

struct HeapReachability {
  double reachability;
  size_t point_idx;
};
// CalDistance() return distance_matrix and modify data to set type to core or not
// distance_matrix form
// ~ a b c d
// a 0 ! ! !
// b 1 0 ! !
// c 1 1 0 !
// d 1 1 1 0
vector<vector<double>> CalDistance (vector<PointPosition> &data) {
  vector<vector<double>> distance_matrix(data.size());
  for (size_t x = 0; x < data.size(); ++x) {
    distance_matrix[x] = vector<double>(x+1, 0);
  }
  for (size_t y = 0; y < data.size(); ++y) {
    for (size_t x = y; x < data.size(); ++x) {
      distance_matrix[x][y] = Distance(data[x], data[y]);
    }
  }
  return distance_matrix;
}
vector<size_t> FindKSmallestNotVisited(const vector<vector<double>> &distance_matrix, const vector<size_t> already_visited, const HeapReachability head,const size_t &min_pts,const double &max_radius) {
  vector<size_t> result(min_pts, SIZE_MAX);
  size_t idx = head.point_idx;
  // Get min_pts shortest distance idx
  for(size_t &i : result) {
    double min = numeric_limits<double>::max();
    size_t min_idx = SIZE_MAX;
    for (size_t x = idx + 1; x < distance_matrix.size(); ++x){
      if (!MyFind(already_visited, x) && !MyFind(result, x)) {
        if (distance_matrix[x][head.point_idx] < min && distance_matrix[x][head.point_idx] < max_radius) {
          min = distance_matrix[x][head.point_idx];
          min_idx = x;
        }
      }
    }
    for (size_t y = idx + 1; y--;){
      if (!MyFind(already_visited, y) && !MyFind(result, y)) {
        if (distance_matrix[head.point_idx][y] < min && distance_matrix[head.point_idx][y] < max_radius) {
          min = distance_matrix[head.point_idx][y];
          min_idx = y;
        }
      }
    }
    i = min_idx;
  }
  return result;
}

vector<HeapReachability> Clustering (vector<PointPosition> &data,const vector<vector<double>> &distance_matrix,const size_t &min_pts,const double &max_radius) {
  vector<size_t> already_visited;
  vector<HeapReachability> heap;
  vector<HeapReachability> result;

  HeapReachability start{numeric_limits<double>::max(), RandomPoint(data.size() - 1)};
  heap.push_back(start);

  while(true){
    already_visited.push_back(heap[0].point_idx);
    result.push_back(heap[0]);

    vector<size_t> reachable_point = FindKSmallestNotVisited(distance_matrix, already_visited, heap[0], min_pts, max_radius);
    double reachable_reachability = Distance(data[heap[0].point_idx], data[reachable_point[reachable_point.size()-1]]);

    heap.erase(heap.begin());

    // Update heap
    auto searchHeap = [&heap] (size_t idx) -> size_t {
      for(size_t i = 0; i < heap.size(); ++i){
        if (heap[i].point_idx == idx){
          return i;
        }
      }
      return SIZE_MAX;
    };
    for (const size_t &i : reachable_point) {
      if (i == SIZE_MAX){
        continue;
      }
      size_t search_result = searchHeap(i);
      if(search_result != SIZE_MAX){
        if(reachable_reachability < heap[search_result].reachability){
          heap[search_result].reachability = reachable_reachability;
        }
      }
      else {
        heap.push_back(HeapReachability{reachable_reachability, i});
      }
    }
    // Sort
    if(!heap.empty()){
      auto sortHeap = [] (vector<HeapReachability> &v){
        for (size_t x = 0; x < v.size() - 1; ++x) {
          for (size_t y = x; y < v.size(); ++y) {
            if (v[y].reachability < v[x].reachability){
              HeapReachability temp = v[y];
              v[y] = v[x];
              v[x] = temp;
            }
          }
        }
      };
      sortHeap(heap);
    }
    else {
      break;
    }
  }
  return result;
}

int main(int argc, char *argv[]){
  clock_t start = clock();
  ifstream data_file;
  data_file.open("data.txt");

  vector<PointPosition> data;

  if (data_file.is_open()) {
    string line;
    while (getline(data_file, line)) {
      PointPosition temp;
      temp = Split(line);
      data.push_back(temp);
    }
  }
  size_t min_pts = stod(argv[1]);
  double max_radius = stod(argv[2]);

  vector<vector<double>> distance_matrix = CalDistance(data);
  vector<HeapReachability> result = Clustering(data, distance_matrix, min_pts, max_radius);

  ofstream data_out("OPTICS/reachability.txt");
  for(const auto &i:result){
    data_out<<i.point_idx<<" "<<fixed<<setprecision(2)<<i.reachability<<endl;
  }
  data_out.close();

  clock_t end = clock();
  double elapsed = (double)(end - start);
  cout << "CPU Cycles: " << elapsed << " cycles" << endl;
  return 0;
}
