import matplotlib.pyplot as plt

def display(dataInput, clusterOutput):
    # Get cluster index
    clusters = []
    not_exception = []
    with open(clusterOutput, "r") as f:
        for line in f:
            line = line.strip().split(' ')
            temp = [int(i) for i in line]
            not_exception += temp
            clusters.append(temp)

    # Get data point index and codinate
    data_point = []
    point_index = []
    with open(dataInput, "r") as f:
        for idx, line in enumerate(f):
            line = line.strip().split(' ')
            temp = [float(i) for i in line]
            data_point.append(temp)
            point_index.append(idx)

    # Get cluster points codinate
    clusters_point = []
    for cluster in clusters:
        x = []
        y = []
        for point in cluster:
            x.append(data_point[point][0])
            y.append(data_point[point][1])
        clusters_point.append([x, y])

    # Draw clusters points
    for i in clusters_point:
        plt.scatter(i[0], i[1])

    # Draw exception points
    exception_x = []
    exception_y = []
    for i in point_index:
        if i not in not_exception:
            exception_x.append(data_point[i][0])
            exception_y.append(data_point[i][1])
    plt.scatter(exception_x, exception_y)
    plt.show()

data_point = []
with open("data.txt", "r") as f:
    x = []
    y = []
    for line in f:
        line = line.strip().split(' ')
        x.append(float(line[0]))
        y.append(float(line[1]))
    data_point = [x, y]
plt.title("Test Sample")
plt.scatter(data_point[0], data_point[1])
plt.show()

plt.title("DBSCAN")
display("data.txt", "DBSCAN/output.txt")
plt.show()


plt.title("Kmeans")
display("data.txt", "Kmeans/output.txt")
plt.show()

plt.title("OPTICS")
display("data.txt", "OPTICS/output.txt")
plt.show()