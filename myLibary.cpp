#pragma once
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <random>
#include <iostream>

using namespace std;

struct PointPosition {
  double x;
  double y;
};
PointPosition Split (string input) {
  size_t pos = input.find(' ');
  PointPosition result;
  result.x = stod(input.substr(0, pos));
  result.y = stod(input.substr(pos, input.size()));
  return result;
}
double Distance (PointPosition d1, PointPosition d2) {
  return sqrt(pow(d1.x - d2.x, 2) + pow(d1.y - d2.y, 2));
}
bool MyFind(vector<size_t> v, size_t f) {
  if (find(v.begin(), v.end(), f) != v.end()) {
    return true;
  }
  return false;
}
size_t RandomPoint(size_t to) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distr(0, to);
  int index = distr(gen);
  return index;
}