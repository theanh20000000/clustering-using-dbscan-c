#!/bin/bash

option=1
while [ $option -ne 0 ]
do
    printf "Clustering demo \n1.Round data \n2.Normal data \n3.Moon data \n0.Exit\nUser Option: "
    read option
    case $option in
        0)
            printf "__Program closed__\n"
        ;;
        1)
            clear
            printf "__Creating and _Running round data__\n"
            python3.11 generate.py make_circles
            printf "_Running DBSCAN\n"
            g++ -w DBSCAN/DBSCAN.cpp && ./a.out 0.15 4 && rm a.out
            printf "_Running Kmeans\n"
            g++ -w Kmeans/kmeans.cpp && ./a.out 2 && rm a.out
            printf "_Running OPTICS\n"
            g++ -w OPTICS/OPTICS.cpp && ./a.out 2 6 && rm a.out
            python3.11 OPTICS/reachability_plot.py
            printf "_Done\n"
            python3.11 display.py
            printf "_____________________________________\n"
            rm data.txt OPTICS/output.txt OPTICS/reachability.txt Kmeans/output.txt DBSCAN/output.txt 
        ;;
        2)
            clear
            printf "__Creating and _Running normal data__\n"
            python3.11 generate.py make_blobs
            printf "_Running DBSCAN\n"
            g++ -w DBSCAN/DBSCAN.cpp && ./a.out 1.1 5 && rm a.out
            printf "_Running Kmeans\n"
            g++ -w Kmeans/kmeans.cpp && ./a.out 5 && rm a.out
            printf "_Running OPTICS\n"
            g++ -w OPTICS/OPTICS.cpp && ./a.out 3 15 && rm a.out
            python3.11 OPTICS/reachability_plot.py
            printf "_Done\n"
            python3.11 display.py
            printf "_____________________________________\n"
            rm data.txt OPTICS/output.txt OPTICS/reachability.txt Kmeans/output.txt DBSCAN/output.txt 
        ;;
        3)
            clear
            printf "__Creating and _Running normal data__\n"
            python3.11 generate.py make_moons
            printf "_Running DBSCAN\n"
            g++ -w DBSCAN/DBSCAN.cpp && ./a.out 0.1 3 && rm a.out
            printf "_Running Kmeans\n"
            g++ -w Kmeans/kmeans.cpp && ./a.out 2 && rm a.out
            printf "_Running OPTICS\n"
            g++ -w OPTICS/OPTICS.cpp && ./a.out 2 6 && rm a.out
            python3.11 OPTICS/reachability_plot.py
            printf "_Done\n"
            python3.11 display.py
            printf "_____________________________________\n"
            rm data.txt OPTICS/output.txt OPTICS/reachability.txt Kmeans/output.txt DBSCAN/output.txt 
        ;;
        *)
            printf "__Input not valid__\n"
        ;;
    esac
done