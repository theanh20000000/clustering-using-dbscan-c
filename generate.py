import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs, make_circles, make_moons
import sys

if sys.argv[1] == "make_blobs":
    X,y = make_blobs(n_samples = 500,n_features = 2,centers = 5)
elif sys.argv[1] == "make_circles":
    X,y = make_circles(n_samples = 500)
elif sys.argv[1] == "make_moons":
    X,y = make_moons(n_samples = 500)

plt.scatter(X[:,0],X[:,1])
plt.show()
with open("data.txt", "w") as f:
    for point in X:
        f.write(str(point[0]) + " " + str(point[1]) + "\n")