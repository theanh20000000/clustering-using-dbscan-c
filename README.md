# Clustering using DBSCAN C++
While the main logic is in C++ the plot are written using python

## Dependency
```
pip install matplotlib scikit-learn
```

## Run
If you are using bash as you shell you can simply run the "start.sh" and see available option<br>
To generate new "data.txt", run below code option = ["make_blobs", "make_circles", "make_moons"]
```
python generate.py <option>
```
To compile C++ you can see in the "start.sh" as an example<br>
After compile "OPTICS.sh" run "reachability_plot.py"<br>
To display the result, run display.py<br>
